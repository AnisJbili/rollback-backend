package fr.aymax.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.aymax.entities.Rollback;
import fr.aymax.service.interfaces.RollbackService;


@RestController
@RequestMapping("/rollback")
public class RollbackController {

	@Autowired
	private RollbackService rollbackService;	
	public RollbackController(RollbackService rollbackService) {
		this.rollbackService = rollbackService;
	};
	
	@PostMapping({ "/", "" })
	public void rollback(@RequestBody Rollback rollback) {
		this.rollbackService.rollback(rollback.getProject(), rollback.getVersion(), rollback.getServer());
	}
	
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String getAllProjects(){
    	return rollbackService.getAllProjects();
    }
    
    @GetMapping("/{project}")
    @ResponseStatus(HttpStatus.OK)
    public String getAllVersions(@PathVariable("project") String project){
    	return rollbackService.getAllVersions(project);
    }
}
