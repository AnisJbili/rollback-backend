package fr.aymax.entities;

public enum  RoleName {
    DEVOPS,
    DEV,
    MANAGER
}