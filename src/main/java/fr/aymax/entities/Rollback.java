package fr.aymax.entities;

import java.io.Serializable;

public class Rollback implements Serializable {
	
	private static final long serialVersionUID = 3160964529678529260L;
	
	private String project;
	private String version;
	private String server;
	
	public Rollback() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Rollback(String project, String version, String server) {
		super();
		this.project = project;
		this.version = version;
		this.server = server ;
	}
	
	public String getProject() {
		return project;
	}
	
	public void setProject(String project) {
		this.project = project;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getServer() {
		return server;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
}
