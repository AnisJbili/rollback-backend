package fr.aymax.service.implementations;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.aymax.service.interfaces.RollbackService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Service
public class RollbackServiceImpl implements RollbackService {

	private RestTemplate restTemplate;

	public RollbackServiceImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public void rollback(String project, String version, String server) {
		try {

			String cmd = "ansible-playbook rollback.yml -e \"SERVER=" + server + " IMAGE_TAG=" + version
					+ "--extra-vars=@vars/" + project + ".yml";

			Runtime run = Runtime.getRuntime();
			Process pr = run.exec(cmd);
			pr.waitFor();
			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getAllProjects() {
		return (restTemplate.getForObject("http://51.77.146.184:5000/v2/_catalog", String.class));
	}

	@Override
	public String getAllVersions(String project) {
		return (restTemplate.getForObject("http://51.77.146.184:5000/v2/aymax/" + project + "/tags/list",
				String.class));
	}
}
