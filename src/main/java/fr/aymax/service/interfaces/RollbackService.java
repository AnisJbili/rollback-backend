package fr.aymax.service.interfaces;

public interface RollbackService {

	public void rollback(String project, String version, String server);
	
	public String getAllProjects();
	
	public String getAllVersions(String project);
}
